import React from 'react'
const GlobalContext = React.createContext({
    globalState:{},
    setGlobalState: () => {},
  });
  
export const GlobalProvider = GlobalContext.Provider
export default GlobalContext