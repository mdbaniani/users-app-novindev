import React, { useContext, useState } from 'react'
import {Input,Label,Button} from 'reactstrap';
import axios from 'axios';
import { useNavigate } from "react-router-dom";
import GlobalContext from '../context/GlobalContext';



export default function Login() {
    const { globalState, setGlobalState } = useContext(GlobalContext);

    const [email,setEmail] = useState('')
    const [password,setPassword] = useState('')
    const [error,setError] = useState('')
    const navigate = useNavigate();

    const doLogin = () =>{
        setError('')
        console.log('about to login',{email : email,password:password})
        axios.post('https://reqres.in/api/login', {
            email: email,
            password: password
        })
        .then(function (response) {
            console.log(response);
            //gather user info
            let userinfo = {
                ...response.data,
                ...{email:email}
            }
            console.log('userinfo',userinfo)
            //save to local storage
            localStorage.setItem("USER",JSON.stringify(userinfo))
            //update state
            setGlobalState(prevState => ({ ...prevState,user:userinfo}))

            //navigate to home
            navigate('/')
        })
        .catch(function (error) {
        console.log(error);
        if(error.response.data.error){
            setError(error.response.data.error)
        }
        });
    }

    return (
        <div style={{display:'flex',marginTop:'4rem',flexDirection:'column',alignItems:'center'}}>
            <Label for="USERNAME" >Email</Label>
            <Input id="USERNAME" style={{width:'15rem'}} value={email} onChange={(e)=>{setEmail(e.target.value)}} placeholder="eg : janet.weaver@reqres.in"/>
            <Label for="PASSWORD" style={{marginTop:'1rem'}}>Password</Label>
            <Input id="PASSWORD" style={{width:'15rem'}} value={password} onChange={(e)=>{setPassword(e.target.value)}}/>
            <p className='text-danger m-2'>{error}</p>
            <Button style={{margin:'2rem'}} onClick={() => {doLogin()}}>Submit</Button>
        </div>
    )
}