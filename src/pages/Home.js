import React, { useContext, useEffect, useState } from 'react'
import { Link } from "react-router-dom";
import GlobalContext from '../context/GlobalContext';

export default function Home() {
    const { globalState, setGlobalState } = useContext(GlobalContext);


    return (
        <div style={{display:'flex',height:'30rem',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
            <h1>Welcome To Users App!</h1>
            <p>You may view all of the users using this app.</p>
            {!globalState.user ? 
                <p>Click <Link to="/login">here</Link> to login.</p> : 
                <p>Click <Link to="/users">here</Link> to view users.</p>
            }
        </div>
    )
}