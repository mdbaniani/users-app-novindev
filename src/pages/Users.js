import React, { useContext, useEffect, useState } from 'react'
import GlobalContext from '../context/GlobalContext';
import { Link } from "react-router-dom";
import axios from 'axios';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
export default function Users() {
    const { globalState, setGlobalState } = useContext(GlobalContext);
    const [users,setUsers] = useState([])
    const [pages,setPages] = useState([])
    const [currentpage,setCurrentpage] = useState(1)
    const [modal, setModal] = useState(false);
    const [currentuser, setCurrentuser] = useState({});


    const toggle = () => setModal(!modal);

    const getUsers = (page) => {

        axios.get(`https://reqres.in/api/users?page=${page}`)
        .then(function (response) {
            // handle success
            console.log(response);
            setUsers(response.data.data)
            //set pages
            let pgs = []
            for(var i=1;i<=response.data.total_pages;i++){
                pgs.push(i)
            }
            console.log('pages:',pgs)
            setPages(pgs)
            setCurrentpage(page)
        })  
        .catch(function (error) {
            // handle error
            console.log(error);
        })
        .finally(function () {
            // always executed
        });
    }

    useEffect(()=>{
        if(globalState.user){
            getUsers(1)
        }
    },[])
    return (
        <div style={{margin:'1rem'}}>
            {!globalState.user ? 
                <p>you are not logged in. Click <Link to="/login">here</Link> to log in.</p> 
                :
                <div>
                    <ul>
                        <p>Click on any user to view user details :</p>
                        {
                            users.map((user,index)=>{
                                return(
                                    <li key={user.id} onClick={()=>{
                                        setCurrentuser(user)
                                        toggle()
                                        }}>
                                        {user.first_name}
                                    </li>
                                )
                            })
                        }
                    </ul>
                    <div>
                        <span>pages :</span>
                        {
                            pages.map((page,index)=>{
                                return(
                                    <span style={{...{margin:'1rem'},...(page === currentpage ? {textDecoration:'underline'}: {})}}
                                        onClick={()=>{
                                            getUsers(page)
                                        }}
                                    >{page}</span>
                                )
                            })
                        }
                    </div>
                    {/* modal */}
                    <Modal isOpen={modal} toggle={toggle} >
                        <ModalBody>
                            <div style={{display:'flex'}}>
                                <img src={currentuser.avatar} />
                                <div>
                                    <p className='m-2'>{`${currentuser.first_name} ${currentuser.last_name}`}</p>
                                    <p className='m-2'>{currentuser.email}</p>
                                </div>
                            </div>
                        </ModalBody>
                        <ModalFooter>
                        {/* <Button color="primary" onClick={toggle}>
                            Do Something
                        </Button>{' '} */}
                        <Button color="secondary" onClick={toggle}>
                            Close
                        </Button>
                        </ModalFooter>
                    </Modal>
                   
                </div>
            }
           
        </div>
    )
}