const utils = {
    getUser : () => {
        let usr = localStorage.getItem('USER')
        // console.log(usr)
        if(usr){
            return JSON.parse(usr)
        }
        else{
            return undefined
        }
    }
}
export default utils