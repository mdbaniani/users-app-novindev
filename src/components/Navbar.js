import React, { useContext, useEffect, useState } from 'react'
import { Link } from "react-router-dom";
import {Navbar as BsNavbar , 
    Collapse,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
} from 'reactstrap';
import GlobalContext from '../context/GlobalContext';
import utils from '../utils';
    
    const NavLinkStyle = {
        textDecoration:'none',
        color:'black'
    }
export default function Navbar() {
    const { globalState, setGlobalState } = useContext(GlobalContext);

    const [isOpen, setIsOpen] = useState(false);    

    const toggle = () => setIsOpen(!isOpen);
    const logoutUser = () =>{
        console.log('logging out user')
        localStorage.removeItem('USER')
        setGlobalState(prevState => ({ ...prevState,user:undefined}))
        window.location.href = '/'
    }

    useEffect(()=>{
        //check user authentication
        let usr = utils.getUser()
        setGlobalState(prevState => ({ ...prevState,user:usr}))
    },[])

    return (
        <BsNavbar color="light" light>
            <NavbarBrand>
                <Link to={''} style={NavLinkStyle}>Users App</Link></NavbarBrand>
            <NavbarToggler  onClick={toggle} className="me-2" />
            <Collapse isOpen={isOpen} navbar>
            <Nav className="me-auto" navbar>
                <NavItem>
                    <NavLink>
                        {!globalState.user ? 
                            <Link to={'login'} style={NavLinkStyle}>Login</Link>                        
                        :   
                            <Link onClick={()=>{logoutUser()}}  style={NavLinkStyle}>Logout</Link>                        
                        }
                        
                    </NavLink>
                </NavItem>
                
                
            </Nav>
            </Collapse>
        </BsNavbar>
    )
}