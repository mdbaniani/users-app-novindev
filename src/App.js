import { BrowserRouter as Router, Route ,Link, Routes} from "react-router-dom";
import Navbar from "./components/Navbar";
import Home from './pages/Home';
import Login from './pages/Login';
import Users from './pages/Users';
import { GlobalProvider } from './context/GlobalContext';
import { useState } from "react";

function App() {

  let initialGlobalState = {
    user:undefined
  }
  
  const [globalState, setGlobalState] = useState(initialGlobalState);
  const initvalue = { 
    globalState, 
    setGlobalState,
  };

  return (
    <GlobalProvider value={initvalue}>
      <div className="App">
        <Router>
          <Navbar />
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path="login" element={<Login />} />
            <Route exact path="users" element={<Users />} />
          </Routes>
        </Router>
      </div>
    </GlobalProvider>
    
  );
}

export default App;
